## Debian Docker container for Ansible playbook and role testing with Molecule.

[![pipeline status](https://gitlab.com/offtechnologies/docker-debian-ansible/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/docker-debian-ansible/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies

[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

This container allows me to test roles and playbooks using Ansible running locally inside the container.
This base image has not only Ansible, but also systemd and sshd, so you can test service management, and pretty much anything you'd be able to test in a full-fledged virtual machine.

> **Important Note**: Use on production servers at your own risk!

## How to Use

First log in to container registry using your username and password i.e:

```bash
docker login registry.company.com
```

Edit your molecule.yaml file i.e:

```yaml
platforms:
  - name: instance
    image: registry.company.com/org.name/docker-debian-ansible:master
    pre_build_image: true
```
You're free to:

```bash
molecule test
```
Enjoy!

## Credits:

[Dockerize an SSH service](https://docs.docker.com/engine/examples/running_ssh_service/)

[Testing your Ansible roles with Molecule](https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule)
